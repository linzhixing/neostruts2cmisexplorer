<%@taglib uri="/struts-tags" prefix="s"%>

<!-- Link to the root -->
<s:if test="%{#session['logged-in'] == 'true'}">
	<div class="row">
		<div class="span4 offset6">
			<s:form method="POST" cssClass="form-search">
				<s:textfield name="term" cssClass="search-query"></s:textfield>
				<s:submit value="%{getText('search')}" action="Search" cssClass="btn primary" />
			</s:form>
		</div>
	</div>
</s:if>
