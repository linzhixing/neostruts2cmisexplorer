<%@taglib uri="/struts-tags" prefix="s"%>

<!-- Link to the root -->
<s:if test="%{#session['logged-in'] == 'true'}">
	<div class="row message">
		<div class="span4">
			<a href="<s:url action="ShowFolder"><s:param name="folderPath">/</s:param></s:url>">
				<s:text name="go_root"/>
			</a>
		</div>
	</div>
</s:if>
