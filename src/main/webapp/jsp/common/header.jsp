<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- CDN css -->
    <link rel="stylesheet" type="text/css" href="http://current.bootstrapcdn.com/bootstrap-v204/css/bootstrap-combined.min.css"></link>
    
    <!-- CDN javascript -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <script type="text/javascript" src="public/javascripts/jquery.jstree"></script>
    
    
    <!-- local css & javascript -->
    <link rel="stylesheet" type="text/css" href="public/stylesheets/jquery-ui-1.8.21.custom.css" />
    <link rel="stylesheet" type="text/css" href="public/stylesheets/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" href="public/stylesheets/base.css" />
    <script type="text/javascript" src="public/javascripts/grid.locale-en.js" ></script>
    <script type="text/javascript" src="public/javascripts/grid.locale-ja.js"></script>
    <script type="text/javascript" src="public/javascripts/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="public/javascripts/jquery.i18n.properties-1.0.9.js" ></script>
    <script type="text/javascript" src="public/javascripts/base.js"></script>
    <script type="text/javascript" src="public/javascripts/bootstrap-dropdown.js"></script>
    
    <!-- Include one of jTable styles. -->
	<link href="public/javascripts/jtable/themes/standard/blue/jtable_blue.css" rel="stylesheet" type="text/css" />
 
	<!-- Include jTable script file. -->
	<script src="public/javascripts/jtable/jquery.jtable.min.js" type="text/javascript"></script>
    
    <title>${title}</title>
    <script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            $('.dropdown-toggle').dropdown();
        });
    })(jQuery);
    </script>
</head>

<body>
<div class="navbar navbar-fixed-top" data-scrollspy="scrollspy">
    <div class="navbar-inner" >
        <div class="container">
            <ul class="nav">
                <li class="active"><a class="brand" href="http://nemakiware.bitbucket.org/index.html" target="_blank">NemakiWare</a></li>
                <li><a href="http://aegif.jp/inquiry.html" target="_blank"><s:text name="contact" /></a></li>
                <s:if test="%{#session['user'] == 'admin'}">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><s:property value="%{getText('console')}" /></a>
                        <ul class="dropdown-menu">
                            <li class="divider"></li>
                            <li><a href="<s:url action="ShowUser"/>"><s:property value="%{getText('console_user')}" /></a></li>
                        	<li><a href="<s:url action="ShowGroup"/>"><s:property value="%{getText('console_group')}" /></a></li>
                        </ul>
                    </li>
                </s:if>
                <s:if test="%{#session['logged-in'] == 'true'}">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><s:property value="#session['user']"/></a>
                        <ul class="dropdown-menu">
                            <li class="divider"></li>
                            <li><a href="<s:url action="Logout"/>">logout</a></li>
                        </ul>
                    </li>
                </s:if>
            </ul>
        </div>
    </div>
</div>

<hr />
<br />

<!-- end tags are in footer.jsp --> 
<div class="container">

