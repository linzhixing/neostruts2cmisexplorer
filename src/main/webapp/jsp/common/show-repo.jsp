<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<s:include value="/jsp/common/header.jsp" />
<s:include value="/jsp/common/search.jsp" />
<s:include value="/jsp/common/root.jsp" />
<s:include value="/jsp/common/navi.jsp" />

<hr />

<div class="row　offset1">
	<s:form method="POST">
		<s:hidden name="parent" value="%{folder}"></s:hidden>
		<s:hidden name="logic" value="add"></s:hidden>
		<s:submit value="%{getText('upload')}"
			action="ShowUpdateDocument"
			cssClass="btn btn-primary btn-large" />
		<s:submit value="%{getText('create_folder')}"
			action="ShowUpdateFolder" cssClass="btn btn-primary btn-large" />
	</s:form>
</div>
<hr />

<!-- Nodes -->
<div class="row">
<table class="nemaki span6">

<s:iterator value="nodes" var="node">
<tr>
	<div class="row">
		<s:if test="#node.folder == true">
			<s:push value="#{'node':node}">
				<s:include value="/jsp/folder/show-folder.jsp" />
			</s:push>
		</s:if>
		<s:else>
			<s:push value="#{'node':node}">
				<s:include value="/jsp/document/show-document.jsp" />
			</s:push>
		</s:else>
	</div>
</tr>
</s:iterator>
</table>
</div>

<hr />
<br clear="all" />
<!-- Previous page / Next page -->

<s:if test="showPrevious == true ">
	<a
		href="
		<s:url action="ShowFolder">
			<s:param name="folder"><s:property value="folder" /></s:param>
			<s:param name="skipCount"><s:property value="previousSkipCount" /></s:param>
		</s:url>
	">
		&lt; Previous </a>
</s:if>
   <s:text name="total" />:&nbsp;
<s:property value="totalNumberOfNodes" />
<s:text name="contents" />
<s:if test="showNext == true ">
	<a
		href="
		<s:url action="ShowFolder">
			<s:param name="folder"><s:property value="folder" /></s:param>
			<s:param name="skipCount"><s:property value="nextSkipCount" /></s:param>
		</s:url>
	">
		Next &gt; </a>
</s:if>


<s:include value="/jsp/common/footer.jsp" />