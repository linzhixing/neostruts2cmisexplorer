<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<s:include value="/jsp/common/header.jsp" />
<s:include value="/jsp/common/root.jsp" />

<!-- Login form -->
<s:if test="loggedIn != true">
	<div class="row">
		<div class="span4 offset2">
			<s:form action="Login" method="POST">
				<s:label for="name" cssClass="label" value="%{getText('user')}"></s:label>
				<s:textfield name="user" maxlength="10" />
				<br />
				<s:label for="password" cssClass="label" value="%{getText('passwd')}"></s:label>
				<s:password name="password" maxlength="10" />
				<br /><br />
				<s:hidden name="folderPath" value="/" />
				<s:submit value="%{getText('login')}" cssClass="btn primary" />
			</s:form>
		</div>
	</div>
</s:if>

<s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if>

<s:include value="/jsp/common/footer.jsp" />
