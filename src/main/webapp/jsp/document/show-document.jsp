<%@taglib uri="/struts-tags" prefix="s"%>

<td>
<div class="span4">

	<a
		href="
                <s:url action='SendFile'>
                    <s:param name='node'><s:property value='id' /></s:param>
                </s:url>
            ">
		<s:push value="#{'mimetype': mimetype}">
			<s:include value="/jsp/document/show-mimetype.jsp" />
		</s:push> <s:property value="name" /> </a>
</div>
</td>

<td>
<div class="span4 offset4">
	<s:text name="size" />:&nbsp;
	<s:property value="humanReadableByteCount(size,true)" />
	<br />
	<s:text name="last_modification" />:&nbsp;
	<s:property value="convertToDate(lastModificationDate)" />
	<br />
	<s:text name="type" />:&nbsp;
	<s:property value="mimetype" />
</div>
</td>

<td>
<div class="span4 offset4">
	<a class="btn"
		href="<s:url action="ShowDocumentDetails">
                <s:param name="parent" value="%{folderId}"></s:param>
                <s:param name="id" value="%{id}"></s:param>
                </s:url>"><s:text
			name="details" /> </a> &nbsp;&nbsp;
	<s:if test="%{#node.collaborator}">
		<a class="btn"
			href="<s:url action="ShowUpdateDocument">
                <s:param name="name" value="%{name}"></s:param>
                <s:param name="parent" value="%{folderId}"></s:param>
                <s:param name="id" value="%{id}"></s:param>
                <s:param name="logic">update</s:param>
            </s:url>"><s:text
				name="update" /> </a>
                &nbsp;&nbsp; <a class="btn"
			href="<s:url action="ShowUpdateDocument">
                <s:param name="name" value="%{name}"></s:param>
                <s:param name="parent" value="%{folderId}"></s:param>
                <s:param name="id" value="%{id}"></s:param>
                <s:param name="logic">delete</s:param>
            </s:url>"><s:text
				name="delete" /> </a>
	</s:if>
	<s:if test="%{#node.owner}">
                &nbsp;&nbsp; <a class="btn"
			href="<s:url action="ShowUpdateContent">
                <s:param name="name" value="%{name}"></s:param>
                <s:param name="parent" value="%{folderId}"></s:param>
                <s:param name="id" value="%{id}"></s:param>
                <s:param name="logic">permission</s:param>
            </s:url>"><s:text
				name="permission" /> </a>
	</s:if>
</div>
</td>

