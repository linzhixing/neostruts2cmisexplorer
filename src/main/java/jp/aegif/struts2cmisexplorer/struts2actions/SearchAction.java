package jp.aegif.struts2cmisexplorer.struts2actions;

import java.util.Iterator;

import jp.aegif.struts2cmisexplorer.domain.Node;
import jp.aegif.struts2cmisexplorer.domain.NodesListPage;
import jp.aegif.struts2cmisexplorer.domain.exceptions.ConnectionFailedException;
import jp.aegif.struts2cmisexplorer.domain.exceptions.NotLoggedInException;
import jp.aegif.struts2cmisexplorer.opencmisbinding.OpenCMISRepositoryClientFacade;
import jp.aegif.struts2cmisexplorer.struts2actions.base.SearchActionBase;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Search content
 * 
 * @author mryoshio
 * 
 */
public class SearchAction extends SearchActionBase {

	private static final long serialVersionUID = 6621205354654725317L;
	private static final Log log = LogFactory.getLog(SearchAction.class);

	/**
	 * Number of results that are skipped when displaying. It is used for paging
	 * results.
	 */
	private int skipCount = 0;

	/**
	 * Information about nodes that are to be displayed on the current page.
	 */
	private NodesListPage page;
	
	
	@Override
	public String execute() throws Exception {
		log.debug("#execute(" + getTerm() + ")");
		query();
		return SUCCESS;
	}

	private void query() throws ConnectionFailedException {
		OpenCMISRepositoryClientFacade facade = getFacade();
		log.debug("facade.getSession: " + facade.getSession());

		//String statement = "SELECT * FROM cmis:document where cmis:name != '"	+ getTerm() + "'";
		String statement = "SELECT * FROM cmis:document where CONTAINS('" + getTerm() + "')";	//TODO GENERATE!
		
		ItemIterable<QueryResult> results = facade.getSession().query(statement,false);
		for (Iterator<QueryResult> it = results.iterator(); it.hasNext();) {
			QueryResult r = it.next();
			Node n = new Node();
			
			n.setId(String.valueOf(r.getPropertyById("id")
					.getFirstValue()));
			n.setType(String.valueOf(r.getPropertyById("type")
					.getFirstValue()));
			n.setName(String.valueOf(r.getPropertyById("name")
					.getFirstValue()));


			//TODO: can't set correct permissions without Nemaki PermissionService getFiltered() method.
			//      getDocuments() method should return all properties?(at present: id, type, name) 
			//Set result's nodes
			//Set filer null, because an inadequate filter hinders getNodeListPage method   
			OperationContext context = getFacade().getSession().getDefaultContext();
			context.setFilter(null);
			Node node = facade.getNode(n.getId());
			getNodes().add(node);
		}
		setCount(results.getPageNumItems());
	}
	
	
	/**
	 * Whether the given object is a folder or not.
	 */
	private static boolean isFolder(CmisObject object) {
		return object.getBaseTypeId().value()
				.equals(ObjectType.FOLDER_BASETYPE_ID);
	}
	
	
	/**
	 *Paging utility 
	 */
	
	/**
	 * The "skip count" to be used if the user clicks on "Previous".
	 */
	public int getPreviousSkipCount() throws NotLoggedInException {
		int previousSkipCount = skipCount
				- getFacade().getMaxItemsPerPage();
		if (previousSkipCount < 0) {
			previousSkipCount = 0;
		}
		return previousSkipCount;
	}

	/**
	 * The "skip count" to be used if the user clicks on "Next".
	 */
	public int getNextSkipCount() throws NotLoggedInException {
		return skipCount + getFacade().getMaxItemsPerPage();
	}

	/**
	 * Whether the "Previous" button is needed.
	 */
	public boolean getShowPrevious() throws NotLoggedInException {
		return skipCount > 0;
	}

	/**
	 * Whether the "Next" button is needed.
	 */
	public boolean getShowNext() throws NotLoggedInException {
		return skipCount + getFacade().getMaxItemsPerPage() < page
				.getTotalNumberOfNodes();
	}
	

	/**
	 * Getters / Setters
	 */
	/*
	public List<Node> getNodes() {
		return page.getNodes();
	}
	*/

	public long getTotalNumberOfNodes() {
		return page.getTotalNumberOfNodes();
	}
	
	public void setSkipCount(int skipCount) {
		this.skipCount = skipCount;
	}
}
